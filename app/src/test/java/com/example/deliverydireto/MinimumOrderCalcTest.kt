package com.example.deliverydireto

import org.junit.Test

import org.junit.Assert.*


class MinimumOrderCalcTest {

    val vm = CartActivityViewModel()

    @Test
    fun calculateTotalTest() {

        val DELTA = 1e-15

        val prices = listOf<Double>(10.0, 20.50, 12.50)
        val expected = 43.0
        val result = vm.calculateTotal(prices)

        assertEquals(expected, result, DELTA)
    }

}