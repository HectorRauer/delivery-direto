package com.example.deliverydireto

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.deliverydireto.adapters.MenuRecyclerAdapter
import com.example.deliverydireto.models.Menu
import kotlinx.android.synthetic.main.activity_main.*

class MenuActivity : AppCompatActivity() {
    private lateinit var menuAdapter: MenuRecyclerAdapter
    private lateinit var mViewModel: MenuActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mViewModel = ViewModelProviders.of(this).get(MenuActivityViewModel::class.java)

        mViewModel.menuResponse.observe(this, Observer {
            initMenuRecyclerView(it)
        })
    }

    private fun initMenuRecyclerView (menu: Menu){
        catRecycler_view.apply {
            layoutManager = LinearLayoutManager(this@MenuActivity)
            menuAdapter = MenuRecyclerAdapter(menu)
            adapter = menuAdapter
        }

    }



}