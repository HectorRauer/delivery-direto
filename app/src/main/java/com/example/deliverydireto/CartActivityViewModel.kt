package com.example.deliverydireto

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.deliverydireto.api.APIrequests
import com.example.deliverydireto.models.Item
import com.example.deliverydireto.repository.CartRepository
import com.example.deliverydireto.room.CartDatabase
import kotlinx.android.synthetic.main.cart_activity.*
import kotlinx.coroutines.*
import retrofit2.Retrofit
import retrofit2.awaitResponse
import retrofit2.converter.gson.GsonConverterFactory

class CartActivityViewModel: ViewModel() {

    val restaurantResponse = CartRepository().getRestaurantInformation()

    val _minimumMsgVisibility = MutableLiveData<Int>()
    val minimumMsgVisibility: LiveData<Int>
        get() = _minimumMsgVisibility

    val _minimumVal = MutableLiveData<Int>()
    val minimumVal: LiveData<Int>
        get() = _minimumVal

    val _emptyMsg = MutableLiveData<Int>()
    val emptyMsg: LiveData<Int>
        get() = _emptyMsg

    val _finishEnabled = MutableLiveData<Boolean>()
    val finishEnabled: LiveData<Boolean>
        get() = _finishEnabled

    val _finishColor = MutableLiveData<Int>()
    val finishColor: LiveData<Int>
        get() = _finishColor



    val  _price = MutableLiveData<String>()
    val price: LiveData<String>
        get() = _price

    val  _priceMsg = MutableLiveData<String>()
    val priceMsg: LiveData<String>
        get() = _priceMsg

    val  _minimumMsg = MutableLiveData<String>()
    val minimumMsg: LiveData<String>
        get() = _minimumMsg



    init {
        _minimumVal.value = 0
        _price.value = "0.0"
        _emptyMsg.value = View.INVISIBLE
        _minimumMsgVisibility.value = View.VISIBLE
        _priceMsg.value = ""

    }


    fun getItemsInCart(context: Context): List<Item> = runBlocking{
        val cart  = CartDatabase.get(context).getCartDao().getListItems()
        return@runBlocking cart
        }

    fun getTotal (context: Context): Double = runBlocking {
        val totalPrice = CartDatabase.get(context).getCartDao().getTotalPrice()
        return@runBlocking calculateTotal(totalPrice)
    }


    fun cleanCart(context: Context){
        viewModelScope.launch {
            CartDatabase.get(context).getCartDao().cleanTable()
        }
    }

    fun deleteItem(context: Context, item: Item){
        viewModelScope.launch {
            CartDatabase.get(context).getCartDao().deleteItem(item)
        }
    }

    fun updatePrice(context: Context, value: String){
        _price.value = value
        _priceMsg.value = context.getString(R.string.total_monetary, price.value!!.toDouble())
    }

    fun updateEmptyMessage(value: Int){
        _emptyMsg.value = value
    }

    fun updateMinimumMessageVisibility(value: Int){
           _minimumMsgVisibility.value = value
        Log.d("VS", "Mudou")
        }

    fun updateMinimumVal(value: Int){
        _minimumVal.value = value
    }

    fun updateMinimumMsg(context: Context){
        if(price.value!!.toDouble() < minimumVal.value!!.toDouble()){
            updateMinimumMessageVisibility(View.VISIBLE)
            _minimumMsg.value = context.getString(R.string.minimum_error, minimumVal.value!!.toDouble())
        }

    }

    fun calculateTotal( items: List<Double>): Double{
        return items.sum()
    }


    fun getMinimumValue(): Double{
        return _minimumVal.value!!.toDouble()
    }

}














