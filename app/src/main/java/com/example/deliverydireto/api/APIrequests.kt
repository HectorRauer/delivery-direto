package com.example.deliverydireto.api

import com.example.deliverydireto.models.Menu
import com.example.deliverydireto.models.Restaurant
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface APIrequests{

    @GET("menu")
    fun getMenu(): Call<Menu>

    @GET("restaurant")
    fun getMinimumOrder(): Call<Restaurant>

    companion object{
        operator fun invoke(): APIrequests{
            return Retrofit.Builder()
                .baseUrl("https://my-json-server.typicode.com/delivery-direto/sample-api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(APIrequests::class.java)
        }
    }
}