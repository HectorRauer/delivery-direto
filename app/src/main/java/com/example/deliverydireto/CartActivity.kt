package com.example.deliverydireto


import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.deliverydireto.adapters.CartListRecyclerViewAdapter
import com.example.deliverydireto.api.APIrequests
import com.example.deliverydireto.databinding.CartActivityBinding
import com.example.deliverydireto.models.Item
import com.example.deliverydireto.models.Restaurant
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.cart_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import retrofit2.awaitResponse
import retrofit2.converter.gson.GsonConverterFactory

class CartActivity: AppCompatActivity() {

    private lateinit var cartAdapter: CartListRecyclerViewAdapter
    private  lateinit var cViewModel: CartActivityViewModel



    val total by lazy { findViewById<TextView>(R.id.total_price) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: CartActivityBinding = DataBindingUtil.setContentView(this, R.layout.cart_activity)
        cViewModel = ViewModelProviders.of(this).get(CartActivityViewModel::class.java)
        binding.cartVM = cViewModel
        binding.lifecycleOwner = this

        //pega o primeiro item escolhido
        val firstItem = cViewModel.getTotal(this)
        cViewModel.updatePrice(this, firstItem.toString())
        val cart = cViewModel.getItemsInCart(this)

        cViewModel.restaurantResponse.observe(this, Observer {
                cViewModel.updateMinimumVal(it.minimum_order_price)
        })


        //setup da UI
       setMinimumOrderHint()

        initCartRecyclerView(cart)
        updateUIValue(this)


        //finalizar pedido
        finish_order.setOnClickListener {
            if(cViewModel.price.value!!.toDouble() < cViewModel.minimumVal.value!!){
                finish_order.isEnabled = false
                Toast.makeText(this,"Valor total abaixo do minimo", Toast.LENGTH_LONG).show()
            }else{
                val gson = GsonBuilder().setPrettyPrinting().create()
                val cartFinal = cViewModel.getItemsInCart(this)
                val finalOrder = gson.toJson(cartFinal)

                cViewModel.cleanCart(this)
                updateUIValue(this)
                cartAdapter.list.removeAll(cart)
                cartAdapter.notifyItemRangeRemoved(0, cart.size)
                empty_cart_layout.visibility = View.VISIBLE
                Toast.makeText(this," Pedido realizado", Toast.LENGTH_LONG).show()
                Log.d("PEDIDO REALIZADO!", "Pedido: $finalOrder")
            }


        }

        bt_addmore.setOnClickListener {
            this.onBackPressed()
        }


    }

    fun initCartRecyclerView(cartRoom: List<Item>){
        rvCart.apply{
            layoutManager = LinearLayoutManager(this@CartActivity)
            cartAdapter = CartListRecyclerViewAdapter(cartRoom, this@CartActivity)
            adapter = cartAdapter


        }

    }

    fun setMinimumOrderHint(){
        cViewModel.restaurantResponse.observe(this, Observer {
            if (cViewModel.price.value!!.toDouble() < it.minimum_order_price.toDouble()){
                min_hint.text = this.getString(R.string.minimum_error, it.minimum_order_price.toDouble())
                cViewModel.updateMinimumVal(it.minimum_order_price)
            }
        })

    }

    fun updateUIValue(context: Context){
        total.text = getString(R.string.total_monetary, cViewModel.getTotal(context))
    }

}



