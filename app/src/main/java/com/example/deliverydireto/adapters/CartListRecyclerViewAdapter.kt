package com.example.deliverydireto.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.deliverydireto.CartActivity
import com.example.deliverydireto.CartActivityViewModel
import com.example.deliverydireto.R
import com.example.deliverydireto.models.Item
import kotlinx.android.synthetic.main.rvcart_items.view.*

class CartListRecyclerViewAdapter (private var items: List<Item>, _activity: CartActivity): RecyclerView.Adapter<RecyclerView.ViewHolder>()  {

    var list = items.toMutableList()
    val activity = _activity
    private lateinit var cViewModel: CartActivityViewModel



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.rvcart_items, parent, false)

        )

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        cViewModel = ViewModelProviders.of(activity).get(CartActivityViewModel::class.java)
        when(holder){
            is ItemViewHolder ->{
                holder.bind(list[position])
                holder.itemView.delete_item.setOnClickListener {
                    //deleta item e atualiza Recycler
                    cViewModel.deleteItem(activity, list[position]) //deleta item do database
                    var newVal = cViewModel.getTotal(holder.itemView.context)
                    this.list.remove(list[position])
                    this.notifyItemRemoved(position)
                    this.notifyItemRangeChanged(position, list.size)
                    cViewModel.updatePrice(holder.itemView.context ,newVal.toString())

                    //Mostra mensagem de carrinho vazio
                    if(list.isEmpty()){
                        cViewModel.updateEmptyMessage(View.VISIBLE)

                    }else{
                        cViewModel.updateEmptyMessage(View.INVISIBLE)
                    }
                    //atualiza hint
                    cViewModel.updateMinimumMsg(holder.itemView.context)

                }

            }
        }
    }



    class ItemViewHolder constructor(
        itemView: View
    ): RecyclerView.ViewHolder(itemView) {


        val item_name: TextView = itemView.cart_item_name
        val item_image: ImageView = itemView.cart_image
        val item_price: TextView = itemView.cart_item_price

        fun bind(cartItems: Item) {
            item_name.text = cartItems.name
            item_price.text = cartItems.price.toString()

            val requestOptions = RequestOptions()
                .placeholder(R.drawable.not_found)
                .error(R.drawable.not_found)

            Glide.with(itemView.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(cartItems.image_url).into(item_image)
        }

    }

}
