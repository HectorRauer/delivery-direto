package com.example.deliverydireto.adapters



import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.deliverydireto.CartActivity
import com.example.deliverydireto.CartActivityViewModel
import com.example.deliverydireto.R
import com.example.deliverydireto.models.Item
import com.example.deliverydireto.room.CartDatabase
import kotlinx.android.synthetic.main.list_items.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ItemListRecyclerViewAdapter (private var items: List<Item>): RecyclerView.Adapter<RecyclerView.ViewHolder>()  {

    private val cViewModel = CartActivityViewModel()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_items, parent, false)
        )

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is ItemViewHolder -> {
                holder.bind(items[position])
                holder.itemView.setOnClickListener {

                    //Abre carrinho
                    val intent = Intent(holder.itemView.context, CartActivity::class.java)
                    startActivity(holder.itemView.context, intent, null)
                    InsertItemInCart(items[position], holder.itemView.context)

                }

            }
        }
    }


    class ItemViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView) {
        val item_name: TextView = itemView.itemTitle
        val item_image: ImageView = itemView.itemImage
        val item_desc: TextView = itemView.itemDescription
        val item_price: TextView = itemView.itemPrice



        fun bind(menuItems: Item) {
            item_name.text = menuItems.name
            if(menuItems.description == null){
                item_desc.text = itemView.context.getText(R.string.none)
            }else{
                item_desc.text = menuItems.description
            }
            item_price.text = itemView.context.getString(R.string.monetary, menuItems.price )

            val requestOptions = RequestOptions()
                .placeholder(R.drawable.not_found)
                .error(R.drawable.not_found)

            Glide.with(itemView.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(menuItems.image_url).into(item_image)
        }

        }

    private fun InsertItemInCart(item: Item,context: Context){
        GlobalScope.launch(Dispatchers.IO){
            CartDatabase.get(context).getCartDao().insertItem(item)


        }

    }


    }
