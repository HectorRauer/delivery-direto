package com.example.deliverydireto.adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.deliverydireto.R
import com.example.deliverydireto.models.MenuCategory
import kotlinx.android.synthetic.main.listmenu_item.view.*

class MenuRecyclerAdapter(private var items: ArrayList<MenuCategory>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {///LIst<Item>

    private lateinit var itemAdapter: ItemListRecyclerViewAdapter

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return MenuViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.listmenu_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is MenuViewHolder ->{
                holder.bind(items[position])

                holder.itemView.itemsRecycler_view.apply {
                    val childLayoutManager = LinearLayoutManager(holder.itemView.itemsRecycler_view.context, RecyclerView.VERTICAL, false)
                    layoutManager =  childLayoutManager
                    itemAdapter = ItemListRecyclerViewAdapter(items[position].items)
                    adapter = itemAdapter
                }
            }
        }
    }


    class MenuViewHolder constructor(
        itemView: View
    ): RecyclerView.ViewHolder(itemView) {
        val item_cat: TextView = itemView.itemCategory


        fun bind(menuCat: MenuCategory){
            item_cat.setText(menuCat.name)


    }
}}