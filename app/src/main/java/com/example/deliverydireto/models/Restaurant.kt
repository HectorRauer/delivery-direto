package com.example.deliverydireto.models

data class Restaurant(
    val delivery_fee: Double,
    val id: Int,
    val minimum_order_price: Int,
    val name: String
)