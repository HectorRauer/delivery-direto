package com.example.deliverydireto.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Item(
    @PrimaryKey
    val id: Int,
    val name: String,
    val price: Double,
    val image_url: String?,
    val description: String?,
    val qt: Int?
)