package com.example.deliverydireto.models

data class MenuCategory(
    val id: Int,
    val items: List<Item>,
    val name: String
)