package com.example.deliverydireto.retrofit

import com.example.deliverydireto.api.APIrequests
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitClient{
    const val  BASE_URL = "https://my-json-server.typicode.com/delivery-direto/sample-api/"
    val retrofitClient: Retrofit.Builder by lazy {
        val okhttpClient = OkHttpClient.Builder()

        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okhttpClient.build())
            .addConverterFactory(GsonConverterFactory.create())
    }
    val apiInterface: APIrequests by lazy {
        retrofitClient
            .build()
            .create(APIrequests::class.java)
    }
}