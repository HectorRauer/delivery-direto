package com.example.deliverydireto.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.deliverydireto.models.Item

@Database(version = 1, entities = [Item::class])
abstract class CartDatabase: RoomDatabase() {
    companion object{
        fun get (context: Context): CartDatabase{
            return Room.databaseBuilder(context, CartDatabase::class.java, "CartItems")
                .build()
        }
    }
     abstract fun getCartDao(): CartDao
}