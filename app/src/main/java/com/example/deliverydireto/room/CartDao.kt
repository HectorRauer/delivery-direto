package com.example.deliverydireto.room

import androidx.room.*
import com.example.deliverydireto.models.Item


@Dao
interface CartDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
     suspend fun insertItem(item: Item)

    @Query("SELECT * FROM Item")
    suspend fun getListItems() : List<Item>

    @Query("SELECT price FROM Item")
    suspend fun getTotalPrice(): List<Double>

    @Query("DELETE FROM Item")
    suspend fun cleanTable()

    @Delete
    suspend fun deleteItem(item: Item)
}