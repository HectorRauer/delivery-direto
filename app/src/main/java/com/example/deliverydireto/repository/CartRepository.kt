package com.example.deliverydireto.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.deliverydireto.api.APIrequests
import com.example.deliverydireto.models.Restaurant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CartRepository {
    fun getRestaurantInformation(): LiveData<Restaurant> {
        val  menuResponse = MutableLiveData<Restaurant>()

        APIrequests().getMinimumOrder()
            .enqueue(object: Callback<Restaurant> {
                override fun onResponse(call: Call<Restaurant>, response: Response<Restaurant>) {
                    if (response.isSuccessful){
                        menuResponse.value = response.body()
                    }
                }

                override fun onFailure(call: Call<Restaurant>, t: Throwable) {
                    t.stackTrace
                }

            })
        return menuResponse
    }
}