package com.example.deliverydireto.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.deliverydireto.api.APIrequests
import com.example.deliverydireto.models.Menu
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MenuRepository {
    fun getMenuInformation(): LiveData<Menu>{
        val  menuResponse = MutableLiveData<Menu>()

        APIrequests().getMenu()
            .enqueue(object: Callback<Menu>{
                override fun onResponse(call: Call<Menu>, response: Response<Menu>) {
                    if (response.isSuccessful){
                        menuResponse.value = response.body()
                    }
                }

                override fun onFailure(call: Call<Menu>, t: Throwable) {
                    t.stackTrace
                }

            })
        return menuResponse
    }
}