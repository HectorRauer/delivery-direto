package com.example.deliverydireto

import androidx.lifecycle.ViewModel
import com.example.deliverydireto.repository.MenuRepository

class MenuActivityViewModel: ViewModel() {

    val menuResponse = MenuRepository().getMenuInformation()
}