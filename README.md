# Projeto Delivery Direto

Projeto simples de um sistema de delivery básico.

## Como funciona?

O projeto consiste em duas telas, uma com o Menu do restaurante e outra com o carrinho de compras. Ao selecionar um item no Menu
o usuário é direcionado diretamente para o carrinho onde pode:
* Adicionar mais items
* Remover items
* Finalizar pedido

Tudo é armazenado em tempo real no database local, ao finalizar o pedido, o estado final do carrinho é logado no formato JSON.

### Ferramentas utilizadas

Kotlin, ROOM (database local), Retrofit2(requisições), Coroutines(multi-threading), Databinding(UI update), MVVM e LiveData (UI update e lifecycle awareness), Testes Unitarios (JUnit)

Tempo de desenvolvimento: 2 dias
